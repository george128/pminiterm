/*
    created by George Verbitsky
    mailto:george128@gmail.com
*/

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/signal.h>
#include <sys/stat.h>
#include <pthread.h>
#include <stdlib.h>
#include <asm/termios.h>
#include <errno.h>

#define ENDMINITERM 4

#define _POSIX_SOURCE 1 /* POSIX compliant source */

#define FALSE 0
#define TRUE 1

/*----------------------------Hack-------------------------------------*/
/* Put the state of FD into *TERMIOS_P.  */
extern int tcgetattr (int __fd, struct termios *__termios_p) __THROW;

/* Set the state of FD to *TERMIOS_P.
   Values for OPTIONAL_ACTIONS (TCSA*) are in <bits/termios.h>.  */
extern int tcsetattr (int __fd, int __optional_actions,
		      const struct termios *__termios_p) __THROW;
/* Flush pending data on FD.
   Values for QUEUE_SELECTOR (TC{I,O,IO}FLUSH) are in <bits/termios.h>.  */
extern int tcflush (int __fd, int __queue_selector) __THROW;

extern int ioctl(int d, int request, ...);

/*----------------------------End Hack-------------------------------------*/


volatile int STOP=FALSE; 
int fd,fd_log;
struct termios oldtio,newtio,oldstdtio,newstdtio;

void *read_thr(void *arg)
{
    int c;
    fd_set rfds;
    struct timeval tv;
    int retval;

    while (STOP==FALSE) /* modem input handler */
    {
	FD_ZERO(&rfds);
	FD_SET(fd, &rfds);
	tv.tv_sec = 0;
	tv.tv_usec = 100000;
	retval = select(fd+1, &rfds, NULL, NULL, &tv);
	if(retval > 0) {
	    if(FD_ISSET(fd,&rfds))
	    {
		switch(read(fd,&c,1)) { /* modem */
		case 0:
		case -1:
		    printf("\n---Disconnected---\n");
		    close(fd);
		    STOP=TRUE;
		    break;
		case 1:
		    if(write(STDOUT_FILENO,&c,1)!=1) { /* stdout */
			printf("Stdout write failed!\n");
		    }
		    break;
		}
	    }
	} else if(retval < 0) {
	    printf("\n---Disconnected---\n");
	    close(fd);
	    STOP=TRUE;
	}
   }
   return NULL;
}

void *write_thr(void *arg)
{
    int c;
    int prev_ch=0;

    fd_set rfds;
    struct timeval tv;
    int retval;

    while (STOP==FALSE) /* modem input handler */
    {
	FD_ZERO(&rfds);
	FD_SET(STDIN_FILENO, &rfds);
	tv.tv_sec = 0;
	tv.tv_usec = 100000;
	retval = select(STDIN_FILENO+1, &rfds, NULL, NULL, &tv);
	if(retval>0 && FD_ISSET(STDIN_FILENO,&rfds))
	{
	    switch(read(0,&c,1)) { /* stdin */
	    case 0:
	    case -1:
		STOP=TRUE;
		break;
	    case 1:
		// printf("c=%d\n",c);
		if(c==ENDMINITERM) {
		    STOP=TRUE;
		    break;
		}
		if(c==10)c=13;	//hack <enter>
		if(prev_ch==0x1b && c==0x4f)c=0x5b; //hack arrows
		if(write(fd,&c,1)!=1) {
		    printf("Write serial failed!\n");
		    STOP=TRUE;
		    break;
		}
		prev_ch=c;
		break;
	    }
	}
   }
    tcsetattr(fd,TCSANOW,&oldtio); /* restore old modem setings */
    tcsetattr(0,TCSANOW,&oldstdtio); /* restore old tty setings */
    close(fd);
//    close(fd_log);
    return NULL;
}

void set_standard_speed(int fd, int br)
{
    tcgetattr(fd,&oldtio); /* save current modem settings */

/* 
  Set bps rate and hardware flow control and 8n1 (8bit,no parity,1 stopbit).
  Also don't hangup automatically and ignore modem status.
  Finally enable receiving characters.
*/
    newtio.c_cflag = br | CS8 | CLOCAL | CREAD;
/*
 Ignore bytes with parity errors and make terminal raw and dumb.
*/
    newtio.c_iflag = IGNPAR;

/*
 Raw output.
*/
    newtio.c_oflag = 0;

/*
 Don't echo characters because if you connect to a host it or your
 modem will echo characters for you. Don't generate signals.
*/
    newtio.c_lflag = 0;
    printf("c_cflag=%02x c_iflag=%02x c_oflag=%02x c_lflag=%02x\n",
	newtio.c_cflag,newtio.c_iflag,newtio.c_oflag,newtio.c_lflag);
/* blocking read until 1 char arrives */
    newtio.c_cc[VMIN]=1;
    newtio.c_cc[VTIME]=0;

/* now clean the modem line and activate the settings for modem */
    tcflush(fd, TCIFLUSH);
    tcsetattr(fd,TCSANOW,&newtio);
/*
  Strange, but if you uncomment this command miniterm will not work
  even if you stop canonical mode for stdout. This is a linux bug.
*/
// tcsetattr(1,TCSANOW,&newtio); /* stdout settings like modem settings */

/* next stop echo and buffering for stdin */
}

void set_non_standard_speed(int fd, int baud)
{
    struct termios2 t;
    
    if (ioctl(fd, TCGETS2, &t))
    {
	perror("TCGETS2");
	exit(-1);
    }

    t.c_cflag &= ~CBAUD;
    t.c_cflag |= BOTHER;
    t.c_ispeed = baud;
    t.c_ospeed = baud;

    if (ioctl(fd, TCSETS2, &t))
    {
	perror("TCSETS2");
        exit(-1);
    }

    if (ioctl(fd, TCGETS2, &t))
    {
	perror("TCGETS2");
	exit(-1);
    }
    if(t.c_ospeed!=baud)
    {
	fprintf(stderr,"Can't set speed %d actual speed: %d\n",baud,t.c_ospeed);
	exit(-1);
    }
}


int main(int argc,char *argv[])
{
    int br=0, br_ns=0;
    long spd;
/* 
  Open modem device for reading and writing and not as controlling tty
  because we don't want to get killed if linenoise sends CTRL-C.
*/
    if(argc<3)
	{fprintf(stderr,"USE: %s <port> <speed>\n",argv[0]);return 10;}

	signal(SIGINT,SIG_IGN);

	switch((spd=atol(argv[2])))
	{
	    case 1200:
		br=B1200;
		break;
	    case 2400:
		br=B2400;
		break;
	    case 4800:
		br=B4800;
		break;
	    case 9600:
		br=B9600;
		break;
	    case 19200:
		br=B19200;
		break;
	    case 38400:
		br=B38400;
		break;
	    case 57600:
		br=B57600;
		break;
	    case 115200:
		br=B115200;
		break;
	    case 230400:
		br=B230400;
		break;
	    case 460800:
		br=B460800;
		break;
	    case 500000:
		br=B500000;
		break;
	    case 576000:
		br=B576000;
		break;
	    case 921600:
		br=B921600;
		break;
	    case 1000000:
		br=B1000000;
		break;
	    case 1152000:
		br=B1152000;
		break;
	    case 1500000:
		br=B1500000;
		break;
	    case 2000000:
		br=B2000000;
		break;
	    case 2500000:
		br=B2500000;
		break;
	    case 3000000:
		br=B3000000;
		break;
	    case 3500000:
		br=B3500000;
		break;
	    case 4000000:
		br=B4000000;
		break;
	    default:
		br_ns=(int)spd;
		//fprintf(stderr, "unknown port speed: %d\n", spd);
		//return -1;
	}


    fd = open(argv[1], O_RDWR | O_NOCTTY);
    if (fd <0) {perror(argv[1]); exit(-1); }

// fd_log = open("serial.log", O_WRONLY|O_CREAT,0644);
// if (fd_log <0) {perror("serial.log"); exit(-1); }
    if(br_ns == 0)
	set_standard_speed(fd, br);
    else
	set_non_standard_speed(fd, br_ns);

    tcgetattr(0,&oldstdtio);
    tcgetattr(0,&newstdtio); /* get working stdtio */
    newstdtio.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(0,TCSANOW,&newstdtio);

/* terminal settings done, now handle in/ouput */
{
    pthread_t ppidRead,ppidWrite;
    if(pthread_create(&ppidRead,NULL,read_thr,0))
    {
	perror("pthread_create");
	exit(10);
    }
    if(pthread_create(&ppidWrite,NULL,write_thr,0))
    {
	perror("pthread_create");
	exit(10);
    }
    while(STOP==FALSE) {
	struct stat statbuf;

	usleep(1000);
	if(stat(argv[1], &statbuf)!=0) {
	    perror(argv[1]);
	    STOP=TRUE;
	}
    }
    pthread_join(ppidWrite,0);
    pthread_join(ppidRead,0);

}
    return 0;
}

