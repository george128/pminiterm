OBJS=pminiterm.o
TARGET=pminiterm
CFLAGS=-O2 -g -Wall
LDFLAGS=-pthread

CC=gcc

$(TARGET): $(OBJS)
	$(CC) $(LDFLAGS) $< -o $@

clean:
	rm -fv $(TARGET) $(OBJS)